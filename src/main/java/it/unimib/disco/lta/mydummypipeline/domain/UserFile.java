package it.unimib.disco.lta.mydummypipeline.domain;

import org.springframework.data.annotation.Id;

public class UserFile {
  @Id
  private String id;
  private String filename;
  private String user_id;

  public String getFilename() {
    return filename;
  }
  public void setFilename(String filename) {
    this.filename = filename;
  }
  public String getUser_id() {
    return user_id;
  }
  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }  
}
